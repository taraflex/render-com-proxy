FROM node:20.17.0-alpine

ARG PORT
ARG RENDER_EXTERNAL_URL

EXPOSE $PORT

WORKDIR /app

ENV PNPM_HOME /root/.local/share/pnpm
ENV PATH /app/node_modules/.bin:$PATH:$PNPM_HOME

COPY *.conf /app/
COPY *.sh /app/
COPY *.json /app/
COPY *.ts /app/
COPY *.mjs /app/
COPY *.yaml /app/
COPY src/ /app/src/

RUN chmod +x /app/run.sh && chmod +x /app/install.sh && /app/install.sh

CMD ["/bin/sh", "/app/run.sh"]
