import { readFileSync, writeFileSync } from 'node:fs';

function BI2Bytes(i) {
    let hex = i.toString(16);
    if (hex.length % 2) {
        hex = '0' + hex;
    }
    return Buffer.from(hex, 'hex');
}

function BIFromBytes(b) {
    return BigInt('0x' + Buffer.from(b.buffer, b.byteOffset, b.byteLength).toString('hex'));
}

const filename = process.argv.at(-1);

let source = readFileSync(filename, 'utf-8')
    //.replace('import * as $protobuf from "protobufjs/minimal"', 'import $protobuf from "protobufjs/minimal.js"')
    .replace(/@property {Uint8Array\|null} (?=\[\w\])/g, '@property {bigint|null} ')
    .replace(/@member {Uint8Array} (?=\w\b)/g, '@member {bigint} ')
    .replace(/(?<=\.\w) = \$util\.newBuffer\(\[\]\)/g, ' = 0n')
    .replace(/\.bytes\(m\.\w\b/g, s => `.bytes(BI2Bytes(m.${s.slice(-1)})`)
    .replace(/(?<=\.\w) = r\.bytes\(\)/g, ' = BIFromBytes(r.bytes())')
    .replace(/while \(r\.pos < c\) \{/g, "if (l) m['__ref'] = r.buf.subarray(r.pos, c);\n        while (r.pos < c) {");

writeFileSync(filename, source + '\n' + BI2Bytes.toString() + '\n\n' + BIFromBytes.toString() + '\n');
