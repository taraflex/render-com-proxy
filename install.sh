#!/usr/bin/env sh

apk add screen wget openssh-keygen dropbear

SSHUSER=dropbear-user
mkdir -p /home/$SSHUSER/.ssh
adduser -s /bin/sh -D $SSHUSER --home /home/$SSHUSER
chown -R $SSHUSER:$SSHUSER /home/$SSHUSER

ssh-keygen -t ed25519 -f ./dropbear-key -N ""
cat ./dropbear-key.pub >> /home/$SSHUSER/.ssh/authorized_keys
mkdir -p /etc/dropbear

npm install --global pnpm rollup
pnpm install
pnpm run build:prod

node . add "default user"

echo "SSH connect key:"
cat ./dropbear-key
rm ./dropbear-key.pub ./dropbear-key