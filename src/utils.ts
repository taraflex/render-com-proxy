import { type Duplex, type Readable, Writable } from 'node:stream';
import { idleTimeout, APPNAME } from './config';
import { type AddressInfo } from 'node:net';
import { STATUS_CODES } from 'node:http';
import paths from 'env-paths';
import { randomInt, createHmac, type KeyObject } from 'node:crypto';
import { type Writer } from 'protobufjs/minimal';
import { node2ProxyError } from './socks5';
import { StringMessage } from './messages';
import { inspect } from 'node:util';

export enum ConnectionType {
    PING = 0,
    HTTP = 1,
    HTTPS = 2,
    SOCKS5 = 5
}

export const PATHS = paths(APPNAME, { suffix: null });
export const X_ID_HEADER = 'x-k-id';
export const EMPTY_OBJECT = Object.create(null);
export const EMPTY_BUFFER = Buffer.alloc(0);
export const kAlive = {
    keepAlive: true,
    keepAliveInitialDelay: idleTimeout
} as const;
export const duplexOpts = {
    allowHalfOpen: true,
    decodeStrings: false,
    objectMode: false,
    readableObjectMode: false,
    writableObjectMode: false,
    emitClose: true,
    autoDestroy: false
} as const;

export function hmac(data: Uint8Array, key: KeyObject): Uint8Array {
    return createHmac('sha512', key).update(data).digest();
}

export function safeDestroy(s: { readonly destroyed: boolean; destroy: (e: any) => void }, e?: any) {
    if (s && !s.destroyed) {
        s.destroy(e);
    }
}

export function encodePb<T>(o: T, encoder: { encode: (m: T, w?: Writer) => Writer }) {
    return encoder.encode(o).ldelim().finish();
}

export const OK_RESPONSE = encodePb({ v: 'ok' }, StringMessage);

const CLOSE_BEFORE_END = new Error('Stream has been closed before ended');
const ALREADY_INVALID = new Error('Stream has been destroyed, errored, or ended before pipelined');

export function noDestructPipeline(signal: AbortSignal, ...streams: [Readable, ...Duplex[], Writable] | [Readable]) {
    return new Promise<void>((fulfill, reject) => {
        function ok() {
            exit(undefined, fulfill);
        }
        function close() {
            exit(CLOSE_BEFORE_END);
        }
        function exit(e: any, cb = reject) {
            signal?.removeEventListener('abort', exit);
            streams.forEach(s => s.off('close', close).off('end', ok).off('error', exit)['pause']?.()['unpipe']?.());
            cb(e);
        }

        if (signal) {
            signal.throwIfAborted();
            signal.addEventListener('abort', exit);
        }

        let prev: Readable;
        for (const s of streams as Duplex[]) {
            if (prev || streams.length < 3) {
                if (!s.readable && !s.writable) {
                    exit(s.errored ?? ALREADY_INVALID);
                    break;
                }
                s.once('close', close).once('end', ok);
            } else if (s.destroyed) {
                exit(s.errored ?? ALREADY_INVALID);
                break;
            }
            if (prev) {
                prev.pipe(s, { end: false });
                prev.resume();
            }
            prev = s.once('error', exit);
        }
    });
}

function readUint32(buf: Uint8Array) {
    let value = 0;
    for (let pos = 0; pos < 5; ++pos) {
        if (pos >= buf.byteLength) return null;
        value = (value | ((buf[pos] & 127) << (7 * pos))) >>> 0;
        if (buf[pos] < 128) return { end: value + pos + 1, start: pos + 1 };
    }
    throw new Error('Invalid protobuf message size prefix');
}

type ProtoMessage<T> = { decode(r: Uint8Array, l?: number): T };

function sizePrefixedParse<T>(chunk: Buffer, parser: ProtoMessage<T>): [T, Buffer] {
    const size = readUint32(chunk);
    if (!size || chunk.byteLength < size.end) return [null, chunk];
    return [parser.decode(chunk.subarray(size.start, size.end)), chunk.subarray(size.end)];
}

export async function waitFor<T>(
    source: Readable,
    parser: ProtoMessage<T> | ((chunk: Buffer) => readonly [T, Buffer]),
    {
        tail,
        signal
    }: {
        tail?: Buffer;
        signal?: AbortSignal;
    } = {}
) {
    const parse: (data: Buffer, parser: ProtoMessage<T>) => readonly [T, Buffer] =
        typeof parser['decode'] === 'function'
            ? sizePrefixedParse
            : (parser as (chunk: Buffer) => readonly [T, Buffer]);

    return new Promise<readonly [T, Buffer]>((fulfill, reject) => {
        signal?.throwIfAborted();

        if (tail?.byteLength) {
            const info = parse(tail, parser as any);
            if (info[0]) return fulfill(info);
        }

        const w = new Writable({
            ...duplexOpts,
            write(chunk, _, next) {
                const info = parse(tail ? Buffer.concat([tail, chunk]) : chunk, parser as any);
                tail = info[1];
                info[0] !== null ? destroy(info, true) : next();
            }
        });

        function destroy(e: any, ok?: true) {
            signal?.removeEventListener('abort', destroy);
            source.off('close', destroy).off('end', destroy).off('error', destroy).pause().unpipe(w);
            w.destroy();
            ok ? fulfill(e) : reject(e);
        }

        signal?.addEventListener('abort', destroy);

        w.once('error', destroy);
        source.once('close', destroy).once('end', destroy).once('error', destroy).pipe(w);
        source.resume();
    });
}

export function getAddress(server: { address: () => AddressInfo | string | null }): string {
    const addr = server.address();
    if (!addr) return '<none>';
    if (addr.constructor === String) {
        return addr;
    }
    const a = addr as AddressInfo;
    return a.address + ':' + a.port;
}

export const enum HTTP {
    OK = 200,
    BAD_REQUEST = 400,
    FORBIDDEN = 403,
    NOT_FOUND = 404,
    METHOD_NOT_ALLOWED = 405,
    PROXY_AUTHENTICATION_REQUIRED = 407,
    REQUEST_ENTITY_TOO_LARGE = 413,
    INTERNAL_SERVER_ERROR = 500,
    NOT_IMPLEMENTED = 501,
    BAD_GATEWAY = 502,
    SERVICE_UNAVAILABLE = 503,
    GATEWAY_TIMEOUT_ERROR = 504
}

export const NOT_ALLOWED = [HTTP.METHOD_NOT_ALLOWED, { Allow: 'GET, HEAD' }] as const;

export const GATEWAY_TIMEOUT_ERROR = new (class extends Error {
    readonly name = 'GATEWAY_TIMEOUT_ERROR';
    valueOf() {
        return HTTP.GATEWAY_TIMEOUT_ERROR;
    }
})();

export function processError(id: string, e: any) {
    let code = e | 0 || node2ProxyError(e) | 0;
    if (!STATUS_CODES[code]) {
        code = HTTP.INTERNAL_SERVER_ERROR;
    }
    if (code !== HTTP.NOT_FOUND && e?.code !== 'ERR_STREAM_PREMATURE_CLOSE') {
        console.warn(
            id || 'Connection info not received',
            '|',
            e instanceof String ? e.toString() : inspect(e, false, 3, true)
        );
    }
    return { code, message: STATUS_CODES[code] };
}

export function setHttpCode(e: any, code: number) {
    if (e == null && STATUS_CODES[code]) {
        e = STATUS_CODES[code];
    }
    try {
        if (!Number.isInteger(e)) {
            if (typeof e === 'string') e = new String(e); // allow change valueOf only on string object not plain string
            e.valueOf = () => code;
        }
    } catch {}
    return e;
}

export function finalizeResponse(soc: Writable, id: string, isSocks: boolean, httpVersion: string, e: unknown) {
    const { code, message } = processError(id, e);
    if (isSocks) {
        soc.end(node2ProxyError(e).payload);
    } else {
        soc.end(
            `HTTP/${httpVersion || '1.1'} ${code} ${message}${
                code === HTTP.SERVICE_UNAVAILABLE ? '\r\nRetry-After: 60' : ''
            }\r\n\r\n`
        );
    }
}

// from https://www.npmjs.com/package/nick-generator
const nick = {
    adjectives: [
        'adorable',
        'amazing',
        'brave',
        'calm',
        'careful',
        'classical',
        'clean',
        'confident',
        'delightful',
        'eager',
        'efficient',
        'electronic',
        'elegant',
        'famous',
        'fancy',
        'fresh',
        'futuristic',
        'gentle',
        'glamorous',
        'handsome',
        'happy',
        'helpful',
        'hungry',
        'impressive',
        'jolly',
        'kind',
        'lively',
        'logical',
        'magnificent',
        'nice',
        'ordinary',
        'perfect',
        'pleasant',
        'practical',
        'pragmatic',
        'proud',
        'rare',
        'reasonable',
        'rich',
        'silly',
        'snowy',
        'sparkling',
        'sunny',
        'suspicious',
        'technical',
        'thankful',
        'unusual',
        'valuable',
        'wicked',
        'witty',
        'wonderful',
        'zealous'
    ],
    animals: [
        'abyssinian',
        'affenpinscher',
        'african-bush-elephant',
        'african-civet',
        'african-clawed-frog',
        'african-forest-elephant',
        'african-palm-civet',
        'african-penguin',
        'african-tree-toad',
        'african-wild-dog',
        'albatross',
        'aldabra-giant-tortoise',
        'alligator',
        'angelfish',
        'ant',
        'anteater',
        'antelope',
        'arctic-fox',
        'arctic-hare',
        'arctic-wolf',
        'armadillo',
        'asian-elephant',
        'asian-palm-civet',
        'australian-mist',
        'avocet',
        'axolotl',
        'aye-aye',
        'baboon',
        'bactrian-camel',
        'badger',
        'balinese',
        'banded-palm-civet',
        'bandicoot',
        'barn-owl',
        'barnacle',
        'barracuda',
        'basking-shark',
        'bat',
        'bear',
        'beaver',
        'bengal-tiger',
        'bird',
        'bison',
        'black-bear',
        'black-rhinoceros',
        'bobcat',
        'bornean-orang-utan',
        'borneo-elephant',
        'bottle-nosed-dolphin',
        'brown-bear',
        'budgerigar',
        'buffalo',
        'bumble-bee',
        'burrowing-frog',
        'butterfly',
        'camel',
        'capybara',
        'caracal',
        'cassowary',
        'caterpillar',
        'centipede',
        'chameleon',
        'cheetah',
        'chinchilla',
        'chinook',
        'chinstrap-penguin',
        'chipmunk',
        'cichlid',
        'clouded-leopard',
        'coati',
        'collared-peccary',
        'common-buzzard',
        'common-frog',
        'coral',
        'cougar',
        'cow',
        'coyote',
        'crane',
        'crested-penguin',
        'crocodile',
        'cross-river-gorilla',
        'curly-coated-retriever',
        'cuttlefish',
        'dachshund',
        'dalmatian',
        'deer',
        'dolphin',
        'dormouse',
        'dragon',
        'dragonfly',
        'duck',
        'dusky-dolphin',
        'dwarf-fortress',
        'eagle',
        'earwig',
        'echidna',
        'egyptian-mau',
        'electric-eel',
        'elephant',
        'elephant-seal',
        'emperor-penguin',
        'emperor-tamarin',
        'emu',
        'falcon',
        'fennec-fox',
        'flamingo',
        'flying-squirrel',
        'fox',
        'frigatebird',
        'frog',
        'fur-seal',
        'galapagos-penguin',
        'galapagos-tortoise',
        'geoffroys-tamarin',
        'gerbil',
        'german-pinscher',
        'giant-clam',
        'gibbon',
        'giraffe',
        'goat',
        'golden-lion-tamarin',
        'goose',
        'gopher',
        'grasshopper',
        'grey-reef-shark',
        'grey-seal',
        'grizzly-bear',
        'guinea-fowl',
        'guinea-pig',
        'guppy',
        'hammerhead-shark',
        'hamster',
        'hare',
        'harrier',
        'havanese',
        'hedgehog',
        'heron',
        'howler-monkey',
        'humboldt-penguin',
        'hummingbird',
        'humpback-whale',
        'hyena',
        'iguana',
        'impala',
        'jackal',
        'jaguar',
        'kakapo',
        'kangaroo',
        'king-penguin',
        'kingfisher',
        'kiwi',
        'koala',
        'komodo-dragon',
        'kudu',
        'lemming',
        'leopard',
        'liger',
        'lion',
        'lionfish',
        'little-penguin',
        'lizard',
        'lynx',
        'macaroni-penguin',
        'macaw',
        'magellanic-penguin',
        'magpie',
        'malayan-civet',
        'malayan-tiger',
        'maltese',
        'mandrill',
        'manta-ray',
        'markhor',
        'masked-palm-civet',
        'mastiff',
        'mayfly',
        'meerkat',
        'millipede',
        'monitor-lizard',
        'monte-iberia-eleuth',
        'moorhen',
        'moose',
        'moray-eel',
        'moth',
        'mountain-lion',
        'mouse',
        'newt',
        'nightingale',
        'ocelot',
        'octopus',
        'okapi',
        'opossum',
        'ostrich',
        'otter',
        'oyster',
        'pademelon',
        'panther',
        'parrot',
        'pekingese',
        'pelican',
        'penguin',
        'pheasant',
        'pied-tamarin',
        'piranha',
        'platypus',
        'pointer',
        'polar-bear',
        'pond-skater',
        'poodle',
        'porcupine',
        'possum',
        'prawn',
        'puffer-fish',
        'puffin',
        'pug',
        'puma',
        'purple-emperor',
        'quail',
        'quetzal',
        'quokka',
        'quoll',
        'rabbit',
        'radiated-tortoise',
        'red-panda',
        'red-wolf',
        'red-handed-tamarin',
        'reindeer',
        'rhinoceros',
        'river-dolphin',
        'river-turtle',
        'rock-hyrax',
        'rockhopper-penguin',
        'roseate-spoonbill',
        'royal-penguin',
        'sabre-toothed-tiger',
        'saint-bernard',
        'salamander',
        'sand-lizard',
        'saola',
        'sea-dragon',
        'sea-otter',
        'sea-slug',
        'sea-squirt',
        'sea-turtle',
        'seahorse',
        'seal',
        'serval',
        'sheep',
        'shrimp',
        'siberian-tiger',
        'silver-dollar',
        'sloth',
        'slow-worm',
        'snapping-turtle',
        'snowshoe',
        'snowy-owl',
        'sparrow',
        'spectacled-bear',
        'sponge',
        'squid',
        'squirrel',
        'stellers-sea-cow',
        'stick-insect',
        'stoat',
        'striped-rocket-frog',
        'sun-bear',
        'swan',
        'tapir',
        'tarsier',
        'tasmanian-devil',
        'tawny-owl',
        'tetra',
        'thorny-devil',
        'tibetan-mastiff',
        'tiffany',
        'tiger',
        'tiger-salamander',
        'tortoise',
        'toucan',
        'tropicbird',
        'tuatara',
        'uakari',
        'uguisu',
        'umbrellabird',
        'vampire-bat',
        'vulture',
        'wallaby',
        'walrus',
        'warthog',
        'water-buffalo',
        'water-dragon',
        'rhinoceros',
        'white-tiger',
        'wildebeest',
        'wolf',
        'woodlouse',
        'woodpecker',
        'wrasse',
        'x-ray-tetra',
        'yak',
        'yellow-eyed-penguin',
        'yorkshire-terrier',
        'zebra',
        'zebu',
        'zonkey',
        'zorse'
    ]
};

export function genNick() {
    return (
        nick.adjectives[randomInt(0, nick.adjectives.length)] + '-' + nick.animals[randomInt(0, nick.animals.length)]
    );
}
