import { createServer, createConnection } from 'node:net';
import { encodePb, PATHS, waitFor } from './utils';
import mkdir from 'make-dir';
import { APPNAME } from './config';
import { existsSync, readdirSync } from 'node:fs';
import EventEmitter from 'node:events';
import { StringMessage } from './messages.mjs';

const IS_WIN = process.platform === 'win32';
const WIN_FILTER = new RegExp('^' + APPNAME + /\\\d+$/.source);

const prefix = IS_WIN ? '//./pipe/' + APPNAME : PATHS.temp + '/ipc';

export function callRemove(userId: string) {
    if (IS_WIN || existsSync(prefix)) {
        const data = encodePb({ v: userId }, StringMessage);
        readdirSync(IS_WIN ? '//./pipe/' : prefix, { withFileTypes: true })
            .filter(IS_WIN ? d => d.isFile() && WIN_FILTER.test(d.name) : d => d.isSocket())
            .map(s => {
                const c = createConnection({ path: s.path + '/' + s.name });
                c.write(data);
                c.destroySoon();
            });
    }
}

export let start = () => {
    if (process.platform !== 'win32') {
        mkdir.sync(prefix);
    }

    const events = new EventEmitter<{ remove: [string] }>();

    createServer(c =>
        c.once('data', async tail => {
            const [{ v }] = await waitFor(c, StringMessage, { tail });
            events.emit('remove', v);
        })
    )
        .on('error', e => console.error('IPC error:', e))
        .listen(prefix + '/' + process.pid);

    start = () => events;
    return events;
};
