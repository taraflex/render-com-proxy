import { readFileSync, unlinkSync } from 'node:fs';
import { Command, InvalidArgumentError, Option, program } from 'commander';
import { name as APPNAME, description as APPDESCRIPTION, version as APPVERSION } from '../package.json';
import { type OutgoingHttpHeaders } from 'node:http2';
import parseHeaders from 'parse-headers';
import { relative } from 'node:path';

export { APPNAME };

const cwd = process.cwd();
Error.prepareStackTrace = (err: Error, stackTraces: NodeJS.CallSite[]) => {
    return (
        String(err) +
        '\n' +
        stackTraces
            .filter(s => {
                const f = s.getFileName();
                return f && !f.startsWith('node:');
            })
            .map(s => {
                return ` • ${[s.getTypeName(), s.getFunctionName()].filter(Boolean).join('.')} (${[
                    relative(cwd, s.getFileName()),
                    s.getLineNumber()
                ]
                    .filter(Boolean)
                    .join(':')})`;
            })
            .join('\n')
    ).trimEnd();
};

const DEFAULT_PORT = 24812;
const DEFAULT_HOST = '127.0.0.1';
const LOG_LEVELS = ['error', 'warn', 'log', 'info'];

export let command = 'client';
export let args: string[];

//todo add descriptions in commands

function act(this: Command, ...a: [...string[], Object, Command]) {
    command = this.name();
    args = (a.slice(0, -2) as string[]).map(s => s?.trim()).filter(Boolean);
}

function intParser(v: string) {
    const t = +v | 0;
    if (t <= 0) {
        throw new InvalidArgumentError('Expected integer > 0');
    }
    return t;
}

const timeoutOption = new Option('-t, --idle-timeout <seconds>', '')
    .env('KAWM_IDLE_TIMEOUT')
    .default(120)
    .argParser(intParser);

const listenOption = new Option('-l, --listen <host/socket>', '')
    .env('KAWM_LISTEN')
    .default({
        host: DEFAULT_HOST,
        port: DEFAULT_PORT,
        toJSON() {
            return DEFAULT_HOST + ':' + DEFAULT_PORT;
        }
    })
    .argParser(v => {
        v = v.trim();
        if (v.startsWith('unix:')) {
            if (process.platform === 'win32') {
                throw new InvalidArgumentError('Unix domain socket is not supported on Windows');
            }
            v = v.slice(5);
            try {
                unlinkSync(v);
            } catch {}
            return { path: v };
        } else {
            try {
                const { port, hostname } = new URL('ftp://' + v);
                return { host: hostname || DEFAULT_HOST, port: +port || (v.endsWith(':21') ? 21 : DEFAULT_PORT) };
            } catch (e) {
                throw new InvalidArgumentError(
                    'Expected localhost[:port] or local_ip[:port] or unix:/unix/domain/socket/path . Port must be an integer [1..65535]'
                );
            }
        }
    });

const serverCommand = new Command('server')
    .description('Start server')
    .addOption(
        new Option('-v, --http-version <version>', '').choices(['1', '2', '2c']).env('KAWM_HTTP_VERSION').default('1')
    )
    .addOption(listenOption)
    .addOption(new Option('-k, --ssl-key <filename>', '').env('KAWM_SSL_KEY'))
    .addOption(new Option('-c, --ssl-cert <filename>', '').env('KAWM_SSL_CERT'))
    .addOption(
        new Option('--access-token-life-time <seconds>', '')
            .env('KAWM_ACCESS_TOKEN_LIFE_TIME')
            .default(15 * 60)
            .argParser(intParser)
    )
    .addOption(
        //todo add port option
        new Option('--allowed-special-ips <masks>', '').env('KAWM_ALLOWED_SPECIAL_IPS').argParser(v =>
            v
                .split(',')
                .map(s => s.trim())
                .filter(Boolean)
        )
    )
    .addOption(timeoutOption);

const clientCommand = new Command('client')
    .description('Start client')
    .addOption(listenOption)
    .addOption(
        new Option('-p, --proxy <url>', '')
            .makeOptionMandatory(true)
            .env('KAWM_PROXY')
            .argParser(v => {
                try {
                    const u = new URL(v.trim());
                    if (u.protocol !== 'http:' && u.protocol !== 'https:')
                        throw 'Only "https://" or "http://" protocols allowed';
                    return u;
                } catch (e) {
                    throw new InvalidArgumentError(e.message || String(e));
                }
            })
    )
    .addOption(
        new Option('-a, --auth <user_id:password>', '')
            .makeOptionMandatory(true)
            .env('KAWM_AUTH')
            .argParser(v => {
                v = v.trim();
                const i = v.indexOf(':');
                const d = i <= 0 ? {} : { uId: v.substring(0, i), password: v.substring(i + 1) };
                if (!d.uId || !d.password) {
                    throw new InvalidArgumentError('Expected "user_id:password"');
                }
                return d;
            })
    )
    .addOption(new Option('--parted').env('KAWM_PARTED').default(false))
    .addOption(new Option('--connect-tries').env('KAWM_CONNECT_TRIES').default(2).argParser(intParser))
    .addOption(
        new Option('-H, --request-headers <headers/filename/stdin>', 'Like curl --header option')
            .env('KAWM_REQUEST_HEADERS')
            .argParser(v => {
                try {
                    v = v.trim();
                    if (v === '@-') {
                        v = readFileSync(process.stdin.fd, 'utf-8');
                    } else if (v[0] === '@') {
                        v = readFileSync(v.slice(1), 'utf-8');
                    }
                    return parseHeaders(v);
                } catch (e) {
                    throw new InvalidArgumentError(e.message || String(e));
                }
            })
    )
    .addOption(timeoutOption);

program
    .name(APPNAME)
    .description(APPDESCRIPTION)
    .version(APPVERSION)
    .addCommand(serverCommand.action(act))
    .addCommand(clientCommand.action(act), { isDefault: true })
    .addCommand(new Command('add').description('Add new user').argument('[comment]').action(act))
    .addCommand(new Command('remove').description('Remove user by id').argument('<id>').action(act))
    .addCommand(
        new Command('edit-comment')
            .description('Edit user comment by id')
            .argument('<id>')
            .argument('<comment>')
            .action(act)
    )
    .addCommand(new Command('list').description('List users').action(act))
    .addCommand(new Command('backup').description('Backup users to stdout').action(act))
    .addCommand(new Command('load').description('Load users from stdin').action(act))
    .addOption(
        new Option('--log-level <level>', '')
            .choices([...LOG_LEVELS, '0', '1', '2', '3'])
            .env('KAWM_LOG_LEVEL')
            .default(2)
            .argParser(v => {
                v = v.trim();
                return LOG_LEVELS.includes(v) ? LOG_LEVELS.indexOf(v) : +v | 0;
            })
    )
    .parse();

let {
    logLevel,
    httpVersion,
    listen,
    parted,
    connectTries,
    proxy,
    idleTimeout,
    sslKey,
    sslCert,
    auth,
    requestHeaders,
    accessTokenLifeTime,
    allowedSpecialIps
} = (command === 'server' ? serverCommand.optsWithGlobals() : clientCommand.optsWithGlobals()) as {
    logLevel: number;
    httpVersion?: '1' | '2' | '2c';
    accessTokenLifeTime?: number;
    listen: { path: string } | { host: string; port: number };
    proxy?: URL;
    parted?: boolean;
    connectTries?: number;
    auth?: { uId: string; password: string };
    idleTimeout: number;
    sslKey?: string;
    sslCert?: string;
    allowedSpecialIps?: string[];
    requestHeaders?: OutgoingHttpHeaders;
};

idleTimeout *= 1000;

export {
    httpVersion,
    listen,
    proxy,
    idleTimeout,
    auth,
    requestHeaders,
    accessTokenLifeTime,
    allowedSpecialIps,
    parted,
    connectTries
};

if (logLevel >= 0) {
    LOG_LEVELS.forEach((v, i) => {
        if (i > logLevel) {
            console[v] = { [v]() {} }[v];
        }
    });
}

export let key =
    httpVersion === '2'
        ? () => {
              const data = readFileSync(sslKey);
              if (!data.byteLength) throw 'Empty file';
              key = () => data;
              return key();
          }
        : null;

export let cert =
    httpVersion === '2'
        ? () => {
              const data = readFileSync(sslCert);
              if (!data.byteLength) throw 'Empty file';
              cert = () => data;
              return cert();
          }
        : null;

let hasError = false;

try {
    cert?.();
} catch (e) {
    console.error('Invalid "ssl-cert". ', e);
    hasError = true;
}

try {
    key?.();
} catch (e) {
    console.error('Invalid "ssl-key". ', e);
    hasError = true;
}

if (hasError) {
    process.exit(9); // 9 - Invalid Argument https://nodejs.org/api/process.html#process_exit_codes
}
