import { Transform, TransformCallback, TransformOptions } from 'node:stream';

let i = 0;

export class Counter extends Transform {
    constructor(readonly prefix = '', public total = 0, options?: TransformOptions) {
        prefix = i.toString(36)[0] + ' ' + prefix;
        ++i;
        super(options);
    }
    _transform(chunk: any, _: BufferEncoding, next: TransformCallback): void {
        this.total += chunk.length;
        console.info(`[${this.prefix}] total: ${this.total}, read: ${chunk.length}`);
        next(null, chunk);
    }
}
