import { ClientHttp2Session, connect, type IncomingHttpStatusHeader, type IncomingHttpHeaders } from 'node:http2';
import { SRPClientSessionStep1, SRPClientSession } from 'tssrp6a';
import { requestHeaders, proxy, auth, idleTimeout, parted, connectTries } from '../config';
import { type IAccessToken, IConnectionMessage, ConnectionMessage, SaltB, AM, MAccessToken } from '../messages';
import { Mutex } from '../mutex';
import { SRP_ROUTINES } from '../user';
import {
    encodePb,
    HTTP,
    waitFor,
    setHttpCode,
    duplexOpts,
    X_ID_HEADER,
    GATEWAY_TIMEOUT_ERROR,
    safeDestroy,
    EMPTY_BUFFER
} from '../utils';
import { BaseProxy } from './BaseProxy';
import { ECONNREFUSED_ERROR } from '../socks5';

type IncomingHeaders = IncomingHttpStatusHeader & IncomingHttpHeaders;

const CLIENT_HEADERS = {
    ...requestHeaders,
    ':path': (proxy && proxy.pathname + proxy.search) || '/',
    ':method': 'POST'
};
const mutex = new Mutex();

let h2client: ClientHttp2Session;
let accessToken: IAccessToken;
let sign: Uint8Array;
let step1: SRPClientSessionStep1;

export class ClientProxy extends BaseProxy {
    protected id: string;
    protected readonly actrl = new AbortController();

    constructor(connectionMessage: IConnectionMessage, tail: Uint8Array = EMPTY_BUFFER) {
        super({
            ...duplexOpts,
            readableHighWaterMark: 1024 * 1024,
            writableHighWaterMark: 1024 * 1024,
            writev: parted
                ? (chunks, next) => {
                      this._write(Buffer.concat(chunks.map(c => c.chunk)), null, next);
                  }
                : undefined
        });
        connectionMessage.uId = auth.uId;
        connectionMessage.accessToken = accessToken;
        connectionMessage.sign = sign;
        const c = encodePb(connectionMessage, ConnectionMessage);
        this.write(tail?.byteLength ? Buffer.concat([c, tail]) : c);
    }

    protected async send(chunk: Uint8Array) {
        console.debug('send: ' + chunk.length);

        if (!h2client || h2client.destroyed || h2client.closed) {
            try {
                h2client?.close();
            } catch {}
            h2client = connect(proxy.origin);
            h2client.on('error', Boolean);
            //h2client.on('error', err => processError('HTTP/2 client error: ', err));
        }

        const headers = this.id ? { ...CLIENT_HEADERS, [X_ID_HEADER]: this.id } : CLIENT_HEADERS;
        if (parted) {
            headers['content-length'] = chunk.byteLength;
        }
        const req = h2client.request(headers, {
            endStream: false,
            signal: this.actrl.signal
        });
        req.setTimeout(idleTimeout, () => safeDestroy(this, GATEWAY_TIMEOUT_ERROR));
        this.client ||= req;

        parted ? req.end(chunk) : req.write(chunk);

        let response: IncomingHeaders;
        await new Promise<void>((fulfill, reject) =>
            req
                .once('response', r => {
                    response = r;
                    if (!this.id) fulfill();
                })
                .once('close', () => reject(ECONNREFUSED_ERROR))
                .once('end', () => (response ? fulfill() : reject(ECONNREFUSED_ERROR)))
                .once('error', reject)
        );

        console.debug('send end: ' + chunk.length);

        return response;
    }

    protected writeNow(chunk: Uint8Array, next?: (_?: Error) => void): boolean | Promise<void> {
        if (this.client && !parted) {
            return next
                ? this.client.write(chunk, next)
                : new Promise<void>((fulfill, reject) =>
                      this.client.write(chunk, err => (err == null ? fulfill() : reject(err)))
                  );
        }
        return next ? this.request(chunk).then(next, next) : this.request(chunk);
    }

    protected async request(chunk: Uint8Array): Promise<undefined> {
        if (this.client) {
            await this.send(chunk);
            return;
        }

        await mutex.waitForUnlock;
        let release = accessToken ? null : await mutex.acquire();

        try {
            const { signal } = this.actrl;

            let headers: IncomingHeaders;
            let connectError: unknown;
            let i = 0;
            do {
                try {
                    headers = await this.send(chunk);
                    break;
                } catch (e) {
                    connectError ||= e;
                    //todo add cancelable backo timeout
                }
            } while (++i < connectTries && !signal.aborted);

            if (!headers) {
                throw connectError;
            }

            const { client } = this;

            const status = headers[':status'];
            this.id = headers[X_ID_HEADER] as string;

            if (status !== HTTP.OK && status !== HTTP.METHOD_NOT_ALLOWED) {
                throw status; //todo .destroy want Error like object (with name field)
            }
            if (parted && !this.id) {
                throw 'Got empty request id'; //todo .destroy want Error like object (with name field)
            }

            try {
                if (status === HTTP.METHOD_NOT_ALLOWED) {
                    accessToken = sign = null;
                    release ||= await mutex.acquire();

                    let [{ s, B }] = await waitFor(client, SaltB, { signal });

                    step1 ||= await new SRPClientSession(SRP_ROUTINES).step1(auth.uId, auth.password);
                    auth.password = null;

                    signal.throwIfAborted();

                    const step2 = await step1.step2(s, B);
                    await this.writeNow(encodePb({ A: step2.A, M: step2.M1 }, AM));

                    const [{ M, accessToken: accessToken1, sign: sign1 }, tail] = await waitFor(client, MAccessToken, {
                        signal
                    });

                    await step2.step3(M);

                    signal.throwIfAborted();

                    accessToken = accessToken1;
                    sign = sign1;

                    if (tail.byteLength) {
                        this.push(tail);
                    }
                }
            } catch (e) {
                throw setHttpCode(e, HTTP.PROXY_AUTHENTICATION_REQUIRED);
            }
            this.pushFromClient();
        } finally {
            release?.();
        }
    }

    _destroy(e: Error, next: (_: Error) => void) {
        this.actrl.abort(e);
        this.client = null;
        next(e);
    }

    _write(chunk: Uint8Array, _, next: (_?: Error) => void) {
        if (this.actrl.signal.aborted) {
            return next(this.actrl.signal.reason); //todo reason must be Error
        }
        this.writeNow(chunk, next);
    }
}
