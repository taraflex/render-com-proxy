import { Duplex } from 'node:stream';
import { safeDestroy } from '../utils';
import { ECONNREFUSED_ERROR, ECONNRESET_ERROR } from '../socks5';

export abstract class BaseProxy extends Duplex {
    protected client: Duplex;

    protected pushFromClient() {
        let err: Error = ECONNREFUSED_ERROR;
        this.client
            .once('error', e => {
                safeDestroy(this, e);
            })
            .once('close', () => {
                err ? safeDestroy(this, err) : this.push(null);
            })
            .once('end', () => {
                err = null;
                this.push(null);
            })
            .on('data', data => {
                err = ECONNRESET_ERROR;
                this.push(data);
            })
            .resume();
    }

    _read() {}

    _final(next: () => void) {
        this.client?.writable ? this.client.end(next) : next();
        this.client = null;
    }
}
