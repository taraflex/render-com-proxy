import { IConnectionMessage } from '../messages';
import { createConnection, isIP, Socket } from 'node:net';
import { ConnectionType, duplexOpts, GATEWAY_TIMEOUT_ERROR, safeDestroy } from '../utils';
import { idleTimeout, allowedSpecialIps } from '../config';
import { DISALLOW_ERROR, Reply } from '../socks5';
import { LookupAddress, LookupOptions } from 'node:dns';
import { lookup } from 'node:dns/promises';
import { isSpecial, createChecker } from 'is-in-subnet';
import { BaseProxy } from './BaseProxy';

let allowedSpecial = createChecker(allowedSpecialIps || []);

function checkIp(ip: string) {
    if (isSpecial(ip) && !allowedSpecial(ip)) {
        throw DISALLOW_ERROR;
    }
}

export class ServerProxy extends BaseProxy {
    protected client: Socket;
    readonly echoSent = false;
    readonly isSocks: boolean;

    constructor(info: IConnectionMessage, tail: Uint8Array) {
        const { hostname } = info;
        if (isIP(hostname)) {
            checkIp(hostname);
        }
        super({
            ...duplexOpts,
            readableHighWaterMark: 1024 * 1024,
            writableHighWaterMark: 1024 * 1024
        });
        this.isSocks = info.type === ConnectionType.SOCKS5;
        this.cork();
        this.client = createConnection(
            {
                port: info.port,
                host: hostname,
                keepAlive: !!info.keepAlive,
                keepAliveInitialDelay: info.keepAlive,
                async lookup(
                    hostname: string,
                    options: LookupOptions,
                    callback: (err: NodeJS.ErrnoException | null, address: string | LookupAddress[]) => void
                ) {
                    try {
                        let a = (await lookup(hostname, options)) || [];
                        if (!Array.isArray(a)) {
                            a = [a];
                        }
                        for (let v of a) {
                            checkIp(v.address);
                        }
                        callback(null, a);
                    } catch (e) {
                        callback(e, null);
                    }
                }
            },
            () => {
                if (this.client) {
                    tail?.byteLength && this.client.write(tail);
                    this.uncork();
                    if (this.isSocks) {
                        this.push(Reply(this.client.localAddress, this.client.localPort));
                    } else if (info.type === ConnectionType.HTTPS) {
                        this.push(Buffer.from(`HTTP/${info.httpVersion || '1.1'} 200 Connection established\r\n\r\n`));
                    }
                    //@ts-ignore
                    this.echoSent = true;
                }
            }
        ).setTimeout(idleTimeout, () => {
            safeDestroy(this, GATEWAY_TIMEOUT_ERROR);
        });

        this.pushFromClient();
    }

    _write(chunk: Buffer, _, next: (_?: Error) => void): void {
        this.client ? this.client.write(chunk, next) : next();
    }

    _destroy(e: Error, next: (_: Error) => void) {
        if (this.client) {
            this.client.destroySoon();
            this.client = null;
        }
        next(e);
    }
}
