import { Duplex } from 'node:stream';
import { decode } from '@leichtgewicht/ip-codec';
import { duplexOpts } from '../utils';

export const enum CMD {
    CONNECT = 0x1,
    BIND = 0x2,
    UDPASSOCIATE = 0x3
}

export const enum ATYPE {
    IPV4 = 0x1,
    HOST = 0x3,
    IPV6 = 0x4
}

// +----+-----+-------+------+----------+----------+
// |VER | CMD |  RSV  | ATYP | DST.ADDR | DST.PORT |
// +----+-----+-------+------+----------+----------+
// | 1  |  1  | X'00' |  1   | Variable |    2     |
// +----+-----+-------+------+----------+----------+

function addrSize(data: Buffer) {
    switch (data[3 + 0]) {
        case ATYPE.IPV4:
            return 4;
        case ATYPE.HOST:
            return 1 + data[3 + 1];
        case ATYPE.IPV6:
            return 16;
    }
}

function extractAddr(data: Buffer) {
    switch (data[3 + 0]) {
        case ATYPE.IPV4:
            return decode(data, 3 + 1, 4);
        case ATYPE.HOST:
            return data.subarray(3 + 2, 3 + data[3 + 1] + 2).toString();
        case ATYPE.IPV6:
            return decode(data, 3 + 1, 16);
    }
}

function extractPort(data: Buffer) {
    return data.readUint16BE(4 + addrSize(data));
}

const enum STATE {
    HANDSHAKE = 0,
    DETECT_REQ_SIZE = 1,
    REQUEST = 2,
    ERROR = 3
}

const HANDSHAKE_OK_RESPONSE = Buffer.from([5, 0]);

// http://www.codenet.ru/webmast/socks51.php
export class Socks5Handshake extends Duplex {
    protected state: STATE = STATE.HANDSHAKE;
    protected buf = Buffer.alloc(0);
    protected readonly rfc1928Actions = {
        [STATE.HANDSHAKE]: {
            // +----+----------+----------+
            // |VER | NMETHODS | METHODS  |
            // +----+----------+----------+
            // | 1  |    1     | 1 to 255 |
            // +----+----------+----------+
            expected: 3,
            next: STATE.DETECT_REQ_SIZE,
            action: (_: Buffer) => {
                // +----+--------+
                // |VER | METHOD |
                // +----+--------+
                // | 1  |   1    |
                // +----+--------+
                this.push(HANDSHAKE_OK_RESPONSE);
                return 3;
            }
        },
        [STATE.DETECT_REQ_SIZE]: {
            expected: 5,
            next: STATE.REQUEST,
            action: (data: Buffer) => {
                this.rfc1928Actions[STATE.REQUEST].expected = 4 + addrSize(data) + 2;
                return 0;
            }
        },
        [STATE.REQUEST]: {
            expected: 0,
            next: STATE.ERROR,
            action: (data: Buffer) => {
                //@ts-ignore
                this.command = {
                    cmd: data[1],
                    hostname: extractAddr(data),
                    port: extractPort(data)
                };
                this.push(null);
                return this.rfc1928Actions[STATE.REQUEST].expected;
            }
        }
    };

    readonly command: { cmd: CMD; hostname: string; port: number };

    constructor(tail: Uint8Array) {
        super({
            ...duplexOpts,
            readableHighWaterMark: 7 + 255,
            writableHighWaterMark: 7 + 255
        });
        tail?.byteLength && this.write(tail);
    }

    _read(_) {}

    _write(chunk: Buffer, _, next: Function) {
        if (this.state === STATE.ERROR) return next(new Error('found Data after command'));

        this.buf = Buffer.concat([this.buf, chunk]);
        while (this.state !== STATE.ERROR && this.buf.length >= this.rfc1928Actions[this.state].expected) {
            if (this.buf[0] != 5) {
                return next(new Error('not socks v5 package'));
            }
            const p = this.rfc1928Actions[this.state];
            this.buf = this.buf.subarray(p.action(this.buf));
            this.state = p.next;
        }
        next();
    }
}
