import { Readable } from 'node:stream';
import { Mutex } from '../mutex';
import { duplexOpts, safeDestroy } from '../utils';
import { ECONNREFUSED_ERROR, ECONNRESET_ERROR } from '../socks5';

export class CombinedReadable extends Readable {
    protected readonly mutex = new Mutex();

    constructor(s: Readable) {
        super(duplexOpts);
        this.add(s).catch(Boolean);
    }

    _read() {}

    async add(s: Readable) {
        const release = await this.mutex.acquire();
        try {
            await new Promise<void>((fulfill, reject) => {
                let err: Error = ECONNREFUSED_ERROR;
                s.once('error', e => reject(e))
                    .once('close', () => (err ? reject(err) : fulfill()))
                    .once('end', () => {
                        err = null;
                        fulfill();
                    })
                    .on('data', data => {
                        err = ECONNRESET_ERROR;
                        this.push(data);
                    })
                    .resume();
            });
        } catch (e) {
            safeDestroy(this, e);
            throw e;
        } finally {
            release();
        }
    }
}
