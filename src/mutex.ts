export class Mutex {
    readonly waitForUnlock = Promise.resolve() as Promise<undefined>;

    async acquire() {
        let f: () => void;
        const prevLock = this.waitForUnlock;
        //@ts-ignore
        this.waitForUnlock = new Promise(fulfill => (f = fulfill));
        await prevLock;
        return f;
    }
}
