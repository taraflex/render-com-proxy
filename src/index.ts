import { httpVersion, listen, proxy, key, cert, command, args } from './config';
import { proxyRequest } from './client';
import { createResponse, appendResponse } from './server';
import {
    createSecureServer as createSecureServerH2,
    createServer as createServerH2C,
    type Http2ServerRequest,
    type Http2ServerResponse
} from 'node:http2';
import { createServer as createServerH1, type IncomingMessage, type ServerResponse } from 'node:http';
import { createServer as createServerTCP } from 'node:net';
import {
    ConnectionType,
    genNick,
    getAddress,
    HTTP,
    kAlive,
    NOT_ALLOWED,
    processError,
    safeDestroy,
    waitFor,
    X_ID_HEADER
} from './utils';
import * as User from './user';
import * as IPC from './ipc';
import { ClientProxy } from './streams/ClientProxy';
import { StringMessage } from './messages.mjs';

switch (command) {
    case 'backup':
        {
            User.backup();
        }
        break;

    case 'load':
        {
            User.load();
        }
        break;

    case 'list':
        {
            console.error('ID    | NAME');
            for (const u of User.list()) {
                console.debug(`${u.uId.padEnd(5)} | ${u.comment || ''}`);
            }
        }
        break;

    case 'add':
        {
            User.generate().then(user => {
                User.set(user.uId, user, args[0] || genNick());
                console.error('Auth Credentials:');
                console.debug(user.uId + ':' + user.password);
            });
        }
        break;

    case 'remove':
        {
            User.remove(args[0]);
            IPC.callRemove(args[0]);
        }
        break;

    case 'edit-comment':
        {
            User.editComment(args[0], args[1]);
        }
        break;

    case 'client':
        {
            const app = createServerTCP(kAlive, soc => soc.once('data', d => proxyRequest(soc, d)))
                .on('error', e => console.error('Local HTTP/SOCKS5 proxy server error:', e))
                .listen(listen, async () => {
                    console.log('Local HTTP/SOCKS5 proxy server started at ' + getAddress(app));

                    const serverAddress = proxy.origin + proxy.pathname + proxy.search;
                    const pinger = new ClientProxy({ type: ConnectionType.PING });
                    try {
                        const [{ v }] = await waitFor(pinger, StringMessage);
                        if (v !== 'ok') {
                            throw 'Invalid response';
                        }
                        console.log(`Remote server "${serverAddress}" is ok`);
                    } catch (e) {
                        processError(`Ping remote server "${serverAddress}" error:`, e);
                    } finally {
                        safeDestroy(pinger);
                    }
                });
        }
        break;

    case 'server':
        {
            IPC.start().on('remove', User.resetCache);

            function handler(req: Http2ServerRequest | IncomingMessage, res: Http2ServerResponse | ServerResponse) {
                try {
                    if (req.method === 'GET' || req.method === 'HEAD') {
                        res.writeHead(HTTP.OK).end();
                    } else if (req.method === 'POST' && !req.complete) {
                        if (req.headers[X_ID_HEADER]) {
                            appendResponse(req, res);
                        } else {
                            createResponse(req, res);
                        }
                    } else {
                        res.writeHead(...NOT_ALLOWED).end();
                    }
                } catch (e) {
                    console.error('Server error:', e);
                    res.writeHead(HTTP.INTERNAL_SERVER_ERROR).end();
                }
            }

            const app = {
                '2': () =>
                    createSecureServerH2(
                        {
                            ...kAlive,
                            key: key(),
                            cert: cert()
                        },
                        handler
                    ),
                //@ts-ignore
                '2c': () => createServerH2C(kAlive, handler),
                '1': () => createServerH1(kAlive, handler)
            }
                [httpVersion]()
                .on('error', e => console.error('Server error:', e))
                .listen(listen, () => console.log('Server started at ' + getAddress(app)));
        }
        break;
}
