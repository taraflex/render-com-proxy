import { encode } from '@leichtgewicht/ip-codec';
import { HTTP } from './utils';
import { ATYPE } from './streams/Socks5Handshake';

enum REPLY {
    SUCCESS = 0x00,
    GENFAIL = 0x01,
    DISALLOW = 0x02,
    NETUNREACH = 0x03,
    HOSTUNREACH = 0x04,
    CONNREFUSED = 0x05,
    TTLEXPIRED = 0x06,
    CMDUNSUPP = 0x07,
    ATYPUNSUPP = 0x08
}

export function Reply(addr: string, port: number, rep = REPLY.SUCCESS) {
    // +----+-----+-------+------+----------+----------+
    // |VER | REP |  RSV  | ATYP | BND.ADDR | BND.PORT |
    // +----+-----+-------+------+----------+----------+
    // | 1  |  1  | X'00' |  1   | Variable |    2     |
    // +----+-----+-------+------+----------+----------+
    let addrBuf = encode(addr);
    return Buffer.from([5, rep, 0, addrBuf.length === 4 ? ATYPE.IPV4 : ATYPE.IPV6, ...addrBuf, port >> 8, port & 255]);
}

function Failed(failtype: REPLY) {
    return Reply('0.0.0.0', 0, failtype);
}

export class ProxyError extends Error {
    readonly payload: Buffer;
    constructor(socks5reply: REPLY, readonly httpCode = HTTP.INTERNAL_SERVER_ERROR) {
        super(REPLY[socks5reply]);
        this.payload = Failed(socks5reply);
    }
    valueOf() {
        return this.httpCode;
    }
}

export const GENFAIL_ERROR = new ProxyError(REPLY.GENFAIL);
export const DISALLOW_ERROR = new ProxyError(REPLY.DISALLOW, HTTP.FORBIDDEN);
export const CMDUNSUPP_ERROR = new ProxyError(REPLY.CMDUNSUPP, HTTP.NOT_IMPLEMENTED);
export const ECONNREFUSED_ERROR = new ProxyError(REPLY.CONNREFUSED, HTTP.BAD_GATEWAY);
export const ECONNRESET_ERROR = new ProxyError(REPLY.GENFAIL, HTTP.BAD_GATEWAY);

const node2ProxyErrorMap = {
    ENETUNREACH: new ProxyError(REPLY.NETUNREACH, HTTP.SERVICE_UNAVAILABLE),

    ENOTFOUND: new ProxyError(REPLY.HOSTUNREACH, HTTP.SERVICE_UNAVAILABLE),
    EHOSTUNREACH: new ProxyError(REPLY.HOSTUNREACH, HTTP.SERVICE_UNAVAILABLE),

    ECONNREFUSED: ECONNREFUSED_ERROR,
    ECONNRESET: ECONNRESET_ERROR
};

export function node2ProxyError(e: unknown) {
    return e instanceof ProxyError ? e : node2ProxyErrorMap[e?.['code']] || GENFAIL_ERROR;
}
