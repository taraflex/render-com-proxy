import { IncomingMessage, type ServerResponse } from 'node:http';
import { AM, ConnectionMessage, type IAccessToken, MAccessToken, SaltB, type IConnectionMessage } from './messages';
import {
    HTTP,
    NOT_ALLOWED,
    processError,
    noDestructPipeline,
    waitFor,
    encodePb,
    hmac,
    setHttpCode,
    ConnectionType,
    finalizeResponse,
    safeDestroy,
    X_ID_HEADER,
    EMPTY_OBJECT,
    OK_RESPONSE
} from './utils';
import { accessTokenLifeTime } from './config';
import { Http2ServerRequest, type Http2ServerResponse } from 'node:http2';
import * as Store from './user';
import { SRPServerSession } from 'tssrp6a';
import { type AuthData, SRP_ROUTINES } from './user';
import { AccessToken } from './messages';
import { type KeyObject, randomInt, timingSafeEqual } from 'node:crypto';
import { ServerProxy } from './streams/ServerProxy';
import { ECONNREFUSED_ERROR } from './socks5';
import { type Readable } from 'node:stream';
import { CombinedReadable } from './streams/CombinedReadable';
import { Counter } from './streams/Counter';

function isValidToken(token: IAccessToken & { __ref?: Uint8Array }, sign: Uint8Array, key: KeyObject) {
    try {
        return (
            sign?.byteLength > 0 && token?.expireAt > Date.now() / 1000 && timingSafeEqual(sign, hmac(token.__ref, key))
        );
    } catch {}
    return false;
}

const rq: Record<string, CombinedReadable> = Object.create(null);

export async function appendResponse(
    req: Http2ServerRequest | IncomingMessage,
    res: ServerResponse | Http2ServerResponse
) {
    //console.debug('part: ' + req.headers['content-length']);
    const id = req.headers[X_ID_HEADER] as string;
    const s = rq[id];
    try {
        if (!s) {
            throw 'parent request not found';
        }
        await s.add(req);
        res.writeHead(HTTP.OK);
    } catch (e) {
        processError(`[${id}}] Partial request error: `, e);
        safeDestroy(s, e);
        res.writeHead(...NOT_ALLOWED);
    } finally {
        //console.debug('part end: ' + req.headers['content-length'], s.isPaused());
        res.writable ? res.end() : safeDestroy(res);
        safeDestroy(req);
    }
}

const SRP_SERVER_SESSION = new SRPServerSession(SRP_ROUTINES);

export async function createResponse(req: Readable, res: ServerResponse | Http2ServerResponse) {
    const OK_HEADERS = Object.create(null);
    let id: string;
    let client: ServerProxy;
    let info: IConnectionMessage = EMPTY_OBJECT;
    let label: string;

    try {
        if ((+(req as IncomingMessage).headers['content-length'] | 0) > 0) {
            do {
                id = randomInt(0, 281474976710655).toString(36);
            } while (rq[id]);
            OK_HEADERS[X_ID_HEADER] = id;
            req = rq[id] = new CombinedReadable(req);
        }

        let [info, tail] = await waitFor(req, ConnectionMessage);

        if (info.type === ConnectionType.PING) {
            label = 'PING';
        } else {
            info.port ||= info.type === ConnectionType.HTTPS ? 443 : 80;
            info.hostname ||= 'localhost';
            label = info.hostname + ':' + info.port;
            console.info(ConnectionType[info.type] + ':', label);
        }

        let auth: AuthData;
        try {
            auth = Store.get(info.uId);
        } catch (e) {
            throw setHttpCode(e, HTTP.FORBIDDEN);
        }

        const {
            abortCtrl: { signal },
            key,
            s,
            v
        } = auth;

        if (isValidToken(info.accessToken, info.sign, key)) {
            res.writeHead(HTTP.OK, OK_HEADERS)['flushHeaders']?.();
        } else {
            let B = await SRP_SERVER_SESSION.step1(info.uId, s, v);

            signal.throwIfAborted();

            res.writeHead(HTTP.METHOD_NOT_ALLOWED, { ...OK_HEADERS, ...NOT_ALLOWED[1] }).write(
                encodePb({ B: B.B, s }, SaltB)
            );

            let [{ A, M }] = await waitFor(req, AM, { signal });
            M = await B.step2(A, M);

            signal.throwIfAborted();

            const accessToken = { expireAt: Date.now() / 1000 + accessTokenLifeTime };

            res.write(
                encodePb(
                    {
                        M,
                        accessToken,
                        sign: hmac(AccessToken.encode(accessToken).finish(), key)
                    },
                    MAccessToken
                )
            );
        }

        if (info.type === ConnectionType.PING) {
            res.end(OK_RESPONSE);
            return;
        }

        await noDestructPipeline(
            signal,
            req,
            new Counter('req -> client'),
            (client = new ServerProxy(info, tail)),
            new Counter('client -> res'),
            res
        );
        if (!client.echoSent) {
            throw ECONNREFUSED_ERROR;
        }
    } catch (e) {
        if (client?.echoSent || !res.writable) {
            processError(label, e);
        } else {
            res.headersSent || res.writeHead(HTTP.OK, OK_HEADERS);
            finalizeResponse(res, label, info.type === ConnectionType.SOCKS5, info.httpVersion, e);
        }
    } finally {
        if (id) {
            delete rq[id];
        }
        res.writable ? res.end() : safeDestroy(res);
        safeDestroy(client);
        safeDestroy(req);
    }
}
