import { type ISaltVerifier, type IUser, SaltVerifier, User } from './messages';
import { existsSync, readdirSync, readFileSync, rmSync, writeFileSync } from 'node:fs';
import crs from 'crypto-random-string';
import { PATHS } from './utils';
import mkdir from 'make-dir';
import { createSecretKey, type KeyObject, randomBytes, randomInt } from 'node:crypto';
import { Reader } from 'protobufjs/minimal';
import { createVerifierAndSalt, SRPParameters, SRPRoutines } from 'tssrp6a';
import { stringToArrayBuffer } from 'tssrp6a/dist/utils';

export type AuthData = ISaltVerifier & { abortCtrl: AbortController; key: KeyObject };

class SRPRoutinesMoreSpeed extends SRPRoutines {
    constructor() {
        super(new SRPParameters(SRPParameters.PrimeGroup[1536], SRPParameters.H.SHA512));
    }
    public computeIdentityHash(I: string, P: string): Promise<ArrayBuffer> {
        return this.hash(stringToArrayBuffer(I + ':' + P));
    }
}

export const SRP_ROUTINES = new SRPRoutinesMoreSpeed();

const root = PATHS.config + '/users/';

let cache: Record<string, AuthData> = Object.create(null);

export async function generate() {
    let uId = crs({
        length: randomInt(1, 6),
        characters: '123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz'
    });
    mkdir.sync(root + uId);
    writeFileSync(root + uId + '/data', '', { flag: 'ax' }); // check if user already exists

    let password = crs({
        length: randomInt(40, 61),
        characters:
            'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzĀāĂăĄąĆćĈĉĊċČčĎďĐđĒēĔĕĖėĘęĚěĜĝĞğĠġĢģĤĥĦħĨĩĪīĬĭĮįİıĲĳĴĵĶķĸĹĺĻļĽľĿŀŁłŃńŅņŇňŉŊŋŌōŎŏŐőŒœŔŕŖŗŘřŚśŜŝŞşŠšŢţŤťŦŧŨũŪūŬŭŮůŰűŲųŴŵŶŷŸŹźŻżŽžſ0123456789_'
    });
    return { password, uId, ...(await createVerifierAndSalt(SRP_ROUTINES, uId, password)) };
}

export function* list() {
    if (existsSync(root)) {
        for (const u of readdirSync(root, { withFileTypes: true })) {
            if (u.isDirectory()) {
                try {
                    yield { uId: u.name, comment: readFileSync(root + u.name + '/comment', 'utf8') };
                } catch {
                    yield { uId: u.name };
                }
            }
        }
    }
}

export function get(uId: string): AuthData {
    if (!cache[uId]) {
        const o = SaltVerifier.decode(readFileSync(root + uId + '/data')) as AuthData;
        o.abortCtrl = new AbortController();
        o.key = createSecretKey(randomBytes(128));
        cache[uId] = o;
    }
    return cache[uId];
}

export function set(uId: string, data: ISaltVerifier | Uint8Array, comment: string) {
    mkdir.sync(root + uId);
    writeFileSync(root + uId + '/data', data instanceof Uint8Array ? data : SaltVerifier.encode(data).finish());
    if (comment) {
        writeFileSync(root + uId + '/comment', comment);
    }
}

export function editComment(uId: string, comment: string) {
    writeFileSync(root + uId + '/comment', comment);
}

export function remove(uId: string) {
    rmSync(root + uId, { recursive: true, force: true });
}

export function resetCache(uId: string) {
    cache[uId]?.abortCtrl.abort();
    delete cache[uId];
    console.log(`Authentication cache reset for user id "${uId}"`);
}

export function backup() {
    for (const u of list()) {
        (u as IUser).data = readFileSync(root + u.uId + '/data');
        process.stdout.write(User.encode(u).ldelim().finish());
    }
}

export function load() {
    const r = Reader.create(readFileSync(process.stdin.fd));
    while (r.pos < r.len) {
        const { uId, data, comment } = User.decode(r, r.uint32());
        set(uId, data, comment);
    }
}
