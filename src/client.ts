import {
    HTTP,
    kAlive,
    processError,
    noDestructPipeline,
    waitFor,
    ConnectionType,
    finalizeResponse,
    safeDestroy,
    EMPTY_OBJECT,
    EMPTY_BUFFER
} from './utils';
import { type Socket } from 'node:net';
import { CMDUNSUPP_ERROR, ECONNREFUSED_ERROR, GENFAIL_ERROR } from './socks5';
import { CMD, Socks5Handshake } from './streams/Socks5Handshake';
import { Duplex } from 'node:stream';
import { IConnectionMessage } from './messages';
import { ClientProxy } from './streams/ClientProxy';
import { Counter } from './streams/Counter';

const HTTPRequestParseRe = /^(?<method>\S+)\s+(?<url>\S+)\s+HTTP\/(?<version>[\d.]+)$/m;

function httpHeadersAwaiter(data: Buffer): [string, Buffer] {
    if (data.byteLength > 16 * 1024) throw HTTP.REQUEST_ENTITY_TOO_LARGE;
    const i = data.lastIndexOf(10);
    if (i > -1 && data[i - 1] === 13 && data[i - 2] === 10 && data[i - 3] === 13) {
        return [data.subarray(0, i + 1).toString('latin1'), data.subarray(i + 1)];
    } else {
        return [null, data];
    }
}

export async function proxyRequest(soc: Socket, tail: Buffer) {
    let info: {
        readonly url: string;
        readonly version?: string;
        readonly method?: string;
    } = EMPTY_OBJECT;
    let client: Duplex;
    let written = soc.bytesWritten;
    let cMessage: IConnectionMessage;
    const isSocks = tail[0] === 5;
    try {
        if (isSocks) {
            await noDestructPipeline(null, soc, (client = new Socks5Handshake(tail)), soc);
            tail = EMPTY_BUFFER;
            written = soc.bytesWritten;

            if (!client['command']) {
                throw GENFAIL_ERROR;
            }

            safeDestroy(client);
            const { cmd, hostname, port } = (client as Socks5Handshake).command;
            info = { url: hostname + ':' + port };

            if (cmd !== CMD.CONNECT) {
                throw CMDUNSUPP_ERROR;
            }

            console.info(ConnectionType[ConnectionType.SOCKS5] + ':', info.url);

            cMessage = {
                hostname,
                port,
                type: ConnectionType.SOCKS5,
                keepAlive: kAlive.keepAliveInitialDelay
            };
        } else {
            let headers: string;

            [headers, tail] = await waitFor(soc, httpHeadersAwaiter, { tail });

            info = headers.match(HTTPRequestParseRe)?.groups as any;
            if (!info) {
                throw HTTP.BAD_REQUEST;
            }
            if (info.url[0] === '/') {
                //not proxy request
                throw HTTP.NOT_FOUND;
            }

            const tls = info.method === 'CONNECT';
            const type = tls ? ConnectionType.HTTPS : ConnectionType.HTTP;

            console.info(ConnectionType[type] + ':', info.url);

            const url = new URL(tls ? 'https://' + info.url : info.url);

            let keepAlive = tls && !/^(?:proxy\-)?connection:\s+close$/im.test(headers);
            if (!keepAlive) {
                soc.setKeepAlive(false);
            }

            if (!tls) {
                headers =
                    `${info.method} ${info.url.slice(url.origin.length)} HTTP/${info.version}\r\nConnection: close` +
                    headers.slice(headers.indexOf('\r\n')).replace(/^(?:proxy\-)?connection:\s+[\w\-,]+/im, '');
                const patchedHeaders = Buffer.from(headers);
                tail = tail.byteLength ? Buffer.concat([patchedHeaders, tail]) : patchedHeaders;
            }

            cMessage = {
                hostname: url.hostname,
                port: +url.port || null,
                type,
                httpVersion: info.version,
                keepAlive: keepAlive ? kAlive.keepAliveInitialDelay : null
            };
        }

        await noDestructPipeline(
            null,
            soc,
            new Counter('soc -> client'),
            (client = new ClientProxy(cMessage, tail)),
            new Counter('client -> soc'),
            soc
        );

        if (soc.bytesWritten === written) {
            throw ECONNREFUSED_ERROR;
        }
    } catch (e) {
        if (soc.writable && soc.bytesWritten === written) {
            finalizeResponse(soc, info.url, isSocks, info.version, e);
        } else {
            processError(info.url, e);
        }
    } finally {
        soc.destroySoon();
        safeDestroy(client);
    }
}
