import externals from 'rollup-plugin-node-externals';
import resolve from '@rollup/plugin-node-resolve';
import json from '@rollup/plugin-json';
import commonjs from '@rollup/plugin-commonjs';
import typescript from 'rollup-plugin-ts';
import alias from '@rollup/plugin-alias';
import { builtinModules } from 'node:module';

const dev = !!process.env.ROLLUP_WATCH;

export default {
    preserveEntrySignatures: false,
    strictDeprecations: true,
    output: {
        indent: false,
        manualChunks: {},
        externalLiveBindings: false,
        generatedCode: { objectShorthand: true, constBindings: true },
        freeze: false,
        format: 'cjs',
        chunkFileNames: '[name].js',
        dir: 'dist',
        sourcemap: dev && 'inline'
    },
    input: {
        index: 'src/index.ts'
    },
    plugins: [
        alias({
            entries: {
                '@protobufjs/inquire': '@stdlib/utils-noop',
                ...Object.fromEntries(builtinModules.map(m => [m, 'node:' + m]))
            }
        }),
        json({ preferConst: true }),
        dev && externals({ devDeps: true }),
        resolve(),
        !dev && commonjs({ defaultIsModuleExports: true }),
        typescript({
            browserslist: false,
            exclude: 'node_modules/**/*',
            tsconfig: c => ({ ...c, noUnusedLocals: !dev, noUnusedParameters: !dev })
        })
    ]
};
